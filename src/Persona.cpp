#include "Persona.h"

Persona::Persona() // constructor por defecto
{
    nombre = "Juan";
    ciudad = "Viedma";
}

Persona::Persona(std::string _nombre, std::string _ciudad, float _efectivo, CajaFuerte* _miCajaFuerte) // constructor parametrizado
{
    nombre = _nombre;
    ciudad = _ciudad;
    efectivo = _efectivo;
    miCajaFuerte = _miCajaFuerte;
}

Persona::~Persona()
{
    //dtor
}

std::string Persona::comoTeLlamas()
{
    return nombre;
}

std::string Persona::dondeVivis()
{
    return ciudad;
}

void Persona::mudarse(std::string unaCiudad)
{
    ciudad = unaCiudad;
}

float Persona::dineroTotal ()
{
    return (efectivo + miCajaFuerte -> cuantoHay ());
}

void Persona::cobrar (float unSueldo)
{
    efectivo += unSueldo * 0.6;
    miCajaFuerte -> guardar (unSueldo * 0.4); // ahorrar el 40% y guardarlo en la caja
}

bool Persona::puedeComprarAlgoQueCuesta (float unValor)
{
    return (this -> dineroTotal() >= unValor);
}

void Persona::reasignarCajaFuerte (CajaFuerte* nuevaCajaFuerte)
{
    miCajaFuerte = nuevaCajaFuerte;
}
